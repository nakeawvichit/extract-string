let testcase1 = '2(acbd3(bav))' // acbdbavbavbav acbdbavbavbav
let testcase2 = '2(a3(b4(c)))' // abccccbccccbcccc abccccbccccbcccc
let testcase3 = '2(ab3(dc4(fgh5(xyz))))' // abdcfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzdcfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzdcfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyzfghxyzxyzxyzxyzxyz * 2
let testcase4 = '13(a)' // aaaaaaaaaaaaa
let testcase5 = ' 2(a4(x))' // axxxx axxxx

let answer = extractStringWithNumber(testcase5)
console.log('Answer:', answer)

/* 
    extractStringWithNumber(str) 

    ทำการคัดแยกตัวอักษร ตัวเลข วงเล็บ ออกและจัดเก็บข้อมูลใหม่ในรูปของ Object
    n = number หรือ จำนวนที่ต้องทำต่อรอบ
    c = character หรือก็คืออชุดตัวอักษร
    { n: 2, c: 'a' }, { n: 3, c: 'b' }, { n: 4, c: 'c' } ]
*/

function extractStringWithNumber(str) {
    console.log('Case: ', testcase1)
    let nums = "";
    let tc = []

    for (let i = 0; i < str.length; i++) {
        if (parseInt(str[i])) {
            nums += parseInt(str[i]).toString()
        } else {
            let item = {}
            let charCode = str[i].charCodeAt()

            if (chkStr(charCode)) {
                item.n = parseInt(nums)

                let s = str[i]

                // recursive จนกว่าจะเจอตัวที่ไม่ใช่ a-z ทำให้สามารถจัดกลุ่มตัวอักษรได้ หากมีมากกว่า 1 ตัวอักษร โดยใช้ charCodeAt() 
                while (chkStr(str[i + 1].charCodeAt())) {
                    if (chkStr(str[i + 1].charCodeAt())) {
                        s += str[i + 1]
                        i++ // next
                    }
                }

                item.c = s

                if (parseInt(nums)) {
                    tc.push(item)
                    nums = ""
                }

            }
        }
    }
    console.log('Data: ', tc)

    /* 
    ลูปจากด้านหลังมาเพื่อทำ String ช่องสุดมาต่อ ใน Node ช่องถัดมา
    */

    let last = ""
    let temp = ""
    for (let b = tc.length - 1; b >= 0; b--) {
        if (b != tc.length - 1) {
            for (let n = 0; n < tc[b].n; n++) {
                if (b != 0) {
                    temp += tc[b].c + last
                } else {
                    temp += tc[b].c + last + ' '
                }
            }
            last = temp
            temp = ""

        } else {
            last += repeatString(tc[b].c, tc[b].n)
        }
    }

    return last
}

function repeatString(str, num) {
    let repeatStr = ''
    while (num > 0) {
        repeatStr += str;
        num--;
    }
    return repeatStr;
}

function chkStr(charCode) {
    if (97 <= charCode && charCode <= 122) { // char code a-z 
        return true
    }
    return false
}